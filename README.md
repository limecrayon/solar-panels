# instructions

visit here for a guide to get set up: https://developers.google.com/custom-search/v1/introduction/

you need to:

1. create a custom search engine (keep track of your cx=value in the html snippet)
2. get an api key

# set up (will be different on windows)

```
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
export GCS_DEVELOPER_KEY=<api-key-from-above>
export GCS_CX=<cx-value-from-above>
```

# run

Get 50 solar panel images and place them in the downloads folder

```
gimages search -q solar+panels -d ./downloads -n 50
```

# rename

To rename the images in the `downloads` directory to another directory, say `downloads-renamed` in a directory, run:

```
python main.py ./downloads ./downloads-renamed
```
