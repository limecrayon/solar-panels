import os
from pathlib import Path
import re
import shutil
from gradio_client import Client


def replace_spaces_and_punctuation(sentence):
    sentence = sentence.replace(" ", "-")
    sentence = re.sub(r"[^\w\s-]", "", sentence)
    return sentence.lower()


def rename_files(target_folder, destination_folder):
    if target_folder == destination_folder:
        raise Exception("target and destination folders must be different")

    client = Client("https://adept-fuyu-8b-demo.hf.space/--replicas/ewgan/")
    downloads = target_folder
    destination_folder_ = Path(destination_folder)
    destination_folder_.mkdir(parents=True, exist_ok=True)

    extensions = [".png", ".jpg", ".jpeg", ".gif", ".bmp", "webp"]
    for file in os.listdir(downloads):
        img_file = None
        for _ in extensions:
            if file.endswith(_):
                img_file = file
                break
        if not img_file:
            continue

        if not os.path.isfile(os.path.join(downloads, file)):
            continue

        result = client.predict(
            os.path.join(downloads, file),
            False,  # bool  in 'Enable detailed captioning' Checkbox component
            fn_index=2,
        )
        new_filename = (
            replace_spaces_and_punctuation(result) + "." + file.split(".")[-1]
        )
        i = 0
        while (destination_folder_ / new_filename).exists():
            new_filename = (
                new_filename.split(".")[0] + str(i) + "." + new_filename.split(".")[-1]
            )
            i += 1

        shutil.copy(Path(downloads) / file, destination_folder_ / new_filename)


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 3:
        print("Usage: python main.py <target_folder_path> <destination_folder_path>")
        sys.exit(1)

    target_folder = sys.argv[1]
    destination_folder = sys.argv[2]

    rename_files(target_folder, destination_folder)
